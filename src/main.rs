use std::net::SocketAddr;

use std::path::PathBuf;
use std::sync::mpsc as std_mpsc;

use warp::filters::ws;
use warp::path::FullPath;
use warp::Filter;

use futures::prelude::*;

use notify::Watcher;

mod serve_files;
use serve_files::get_file;

mod websocket;

fn main() {
    let (tx, rx) = std_mpsc::channel();

    let mut watcher = notify::watcher(tx, std::time::Duration::from_millis(500)).unwrap();
    watcher
        .watch("./target/doc/arrayvec/", notify::RecursiveMode::Recursive)
        .unwrap();

    serve("./target/doc/", "127.0.0.1:8000".parse().unwrap(), rx);
}

fn serve(
    document_path: impl Into<PathBuf>,
    addr: SocketAddr,
    reload_channel: std_mpsc::Receiver<notify::DebouncedEvent>,
) {
    let socket_map = websocket::new_socketmap();
    let socketmap_filter = {
        let socket_map = socket_map.clone();
        warp::any().map(move || socket_map.clone())
    };

    let document_path = document_path.into();

    let files = warp::path::full()
        .map(move |request_path: FullPath| {
            println!("Serving file");
            get_file(&document_path, request_path.as_str())
        });

    let websocket = warp::ws2()
        .and(warp::path::full())
        .and(socketmap_filter)
        .map(move |ws: ws::Ws2, path: FullPath, socket_map| {
            let path = path.as_str().to_string();

            println!("{} connected to websocket", path);

            ws.on_upgrade(move |websocket| {
                websocket::websocket_connected(websocket, path, socket_map)
            })
        });

    let routes = websocket.or(files);

    tokio::run(futures::future::lazy(move || {
        tokio::spawn(warp::serve(routes).bind(addr));

        tokio::spawn(
            futures::stream::poll_fn(move || {
                // TODO: Figure out why this is only happening once
                println!("Got poll");

                match reload_channel.try_recv() {
                    Ok(event) => Ok(Async::Ready(Some(event))),
                    Err(std_mpsc::TryRecvError::Empty) => Ok(Async::NotReady),
                    Err(std_mpsc::TryRecvError::Disconnected) => Ok(Async::Ready(None)),
                }
            })
            .for_each(move |event| {
                println!("{:?}", event);

                socket_map.lock().unwrap().iter().for_each(|(_k, v)| {
                    if let Err(e) = v.unbounded_send(ws::Message::text("reload")) {
                        eprintln!("Websocket error: {}", e);
                    }
                });

                futures::future::ok(())
            }),
        );

        futures::future::ok(())
    }));
}

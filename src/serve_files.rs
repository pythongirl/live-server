use std::path::Path;
use std::io::{Result, Read};

use http::Response;


fn open_file(p: &Path) -> Result<Vec<u8>> {
    let mut contents = Vec::new();
    std::fs::File::open(p)?.read_to_end(&mut contents)?;
    Ok(contents)
}

// TODO: Change the string type to an HTTP Response with error handleing
fn read_file(path: &Path) -> Response<Vec<u8>> {
    if !path.exists() {
        return Response::builder()
            .status(404)
            .header("Content-Type", "text/plain")
            .body(b"Not Found".to_vec())
            .unwrap();
    }

    match open_file(path) {
        Ok(contents) => {
            let mime_type = mime_guess::from_path(path).first_or_octet_stream();
            let mime_string = mime_type.as_ref();

            if mime_string == "text/html" {
                // TODO: Inject the live reload code into contents
            }

            Response::builder()
                .status(200)
                .header("Content-Type", mime_string)
                .body(contents)
                .unwrap()
        },
        Err(e) => Response::builder()
            .status(500)
            .header("Content-Type", "text/plain")
            .body(format!("500: Internal Server Error\n\n{}", e).as_bytes().to_owned())
            .unwrap(),
    }
}

fn serve_404(root_path: &Path) -> Response<Vec<u8>> {
    let mut res = read_file(&root_path.join("404.html"));
    *res.status_mut() = http::StatusCode::NOT_FOUND;
    res
}

pub fn get_file(root_path: &Path, request_path: impl AsRef<Path>) -> Response<Vec<u8>> {
    let request_path = request_path.as_ref();

    let file_path = root_path.join(request_path.strip_prefix("/").unwrap_or(request_path));

    if file_path.exists() {
        if file_path.is_file() {
            read_file(&file_path)
        } else if file_path.is_dir() {
            let index_file = &file_path.join("index.html");

            if index_file.exists() {
                read_file(index_file)
            } else {
                serve_404(&root_path)
            }
        } else {
            // TODO: Change to a better status code and error
            Response::builder()
                .status(500)
                .body(b"File could not be served".to_vec())
                .unwrap()
        }
    } else if file_path.with_extension("html").is_file() {
        // TODO: Figure out what this extension is called
        read_file(&file_path.with_extension("html"))
    } else {
        serve_404(&root_path)
    }
}
